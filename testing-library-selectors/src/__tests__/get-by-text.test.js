import { render, screen } from "@testing-library/react";
import App from "../App";

describe("Get By Text testing suite", () => {
  test("Should find a specific text", (done) => {
    render(<App />);
    const awesomeText = screen.getByText(/I'm an awesome text/i);
    expect(awesomeText).toBeInTheDocument();
    done();
  });

  test("Should find a hidde text", (done) => {
    render(<App />);
    const awesomeText = screen.getByText(/I'm a hidden text/i);
    expect(awesomeText).toBeInTheDocument();
    expect(awesomeText).toHaveStyle({ display: "none" });
    done();
  });
});
