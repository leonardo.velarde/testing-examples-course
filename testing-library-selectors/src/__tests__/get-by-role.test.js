import { render, screen } from "@testing-library/react";
import App from "../App";

describe("Get By Role testing suite", () => {
  test("Should find a button element", (done) => {
    render(<App />);
    const button = screen.getByRole("button", { name: "Click me!" });
    expect(button).toBeInTheDocument();
    done();
  });

  test("Sould find a text input", (done) => {
    render(<App />);
    const textInput = screen.getByRole("textbox");
    expect(textInput).toBeInTheDocument();
    done();
  });

  test("Should find a hidded button", (done) => {
    render(<App />);
    const textInput = screen.getByRole("button", {
      hidden: true,
      name: "Open dialog",
    });
    expect(textInput).toBeInTheDocument();
    done();
  });

  test("Should find a selected tab", (done) => {
    render(<App />);
    const textInput = screen.getByRole("tab", { selected: true });
    expect(textInput).toBeInTheDocument();
    done();
  });

  test("Should find a checked checkbox", (done) => {
    render(<App />);
    const textInput = screen.getByRole("checkbox", { checked: true });
    expect(textInput).toBeInTheDocument();
    done();
  });
});
