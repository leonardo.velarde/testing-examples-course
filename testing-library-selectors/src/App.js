
function App() {
  return (
   <>
    <button> Click me! </button>
    <input type="text"  value="write some awesome" onChange={console.log} />

    <main aria-hidden="true">
      <button>Open dialog</button>
    </main>


    <div role="tablist">
      <button role="tab" aria-selected="true">Native</button>
      <button role="tab" aria-selected="false">React</button>
      <button role="tab" aria-selected="false">Cypress</button>
    </div>

    <section>
      <button role="checkbox" aria-checked="true">Sugar</button>
      <button role="checkbox" aria-checked="false">Gummy bears</button>
      <button role="checkbox" aria-checked="false">Whipped cream</button>
    </section>

    <p> I'm an awesome text </p>

    <p style={{ display: 'none' }}> I'm a hidden text </p>

   </>
  );
}

export default App;
